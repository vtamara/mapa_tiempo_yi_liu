
export const EMPTY = '—';
export const CHINA_MAP1 = 'CHN1';
export const CHINA_MAP2 = 'CHN2';
export const GLOBAL_EN = 'Global';
export const HONGKONG_MAP = 'HKG';
export const ITALY_MAP = 'ITA';
export const ITALY_MAP2 = 'ITA2';
export const TRANSMISSION = 'TRANSMISSION';
export const US_MAP = 'US';
export const US_MAP2 = 'US2'
export const WORLD_MAP = 'WORLD';

export const CHINA_ZH = '中国';
export const COLOMBIA_ZH = '哥伦比亚';
export const DENMARK_ZH = '丹麦';
export const DIAMOND_PRINCESS_ZH = '钻石公主号';
export const FRANCE_ZH = '法国';
export const GLOBAL_ZH = '全球';
export const HONGKONG_ZH = '香港';
export const ITALY_ZH = '意大利';
export const MAINLAND_CHINA_ZH = '中国大陆';
export const NETHERLANDS_ZH = '荷兰';
export const TAIWAN_ZH = '台湾';
export const UK_ZH = '英国';
export const US_ZH = '美国';

